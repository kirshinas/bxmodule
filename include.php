<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */

include_once __DIR__ . '/vendor/autoload.php';

\kirshinas\helpers\Image::$rootPath = dirname(dirname(dirname(__DIR__)));
\kirshinas\helpers\Image::$cachePath = '/bitrix/cache/resize_images/';

