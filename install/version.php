<?php
/**
 * @author Kirshin Alexandr <kirshin.as@gmail.com>
 */
$arModuleVersion = array(
    "VERSION"      => "1.0.0",
    "VERSION_DATE" => "2014-07-4 00:00:00"
);