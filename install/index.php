<?php

/**
 * @author Kirshin Alexandr <kirshin.as@gmail.com>
 */
class app extends CModule
{
    var $MODULE_ID;
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;

    function app()
    {
        $arModuleVersion = array();
        include("version.php");
        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }
        $this->MODULE_ID = basename(dirname(__DIR__));
        $this->MODULE_NAME = 'Модуль сайта';
        $this->MODULE_DESCRIPTION = "Модуль сайта";
        $this->PARTNER_NAME = '';
        $this->PARTNER_URI = '';
    }

    function DoInstall()
    {

        RegisterModule($this->MODULE_ID);
    }

    function DoUninstall()
    {

        UnRegisterModule($this->MODULE_ID);
    }
}